# Copyright 2013 Thomas Witt
# Copyright 2014 Jonathan Dahan <jonathan@jonathan.is>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=none multibuild=false ] cmake [ api=2 ]

SUMMARY="Software radio development toolkit"
DESCRIPTION="
GNU Radio is a free software development toolkit that provides the signal
processing runtime and processing blocks to implement software radios using
readily-available, low-cost external RF hardware and commodity processors. It
is widely used in hobbyist, academic and commercial environments to support
wireless communications research as well as to implement real-world radio
systems.
"
HOMEPAGE="https://gnuradio.org"
DOWNLOADS="${HOMEPAGE}/releases/${PN}/${PNV}.tar.gz"

BUGS_TO="pyromaniac@thwitt.de"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    grc [[ description = [ build the gnuradio companion and grc modules ]
           requires = [ python_abis: 2.7 ] ]]
    qt4 [[ description = [ build the Qt GUI module ] ]]
"

DEPENDENCIES="
    build+run:
        dev-lang/swig[>=3.0.0][python]
        dev-libs/boost[>=1.35]
        dev-libs/orc:0.4[>=0.4.11]
        dev-python/Cheetah[>=2.0.0][python_abis:*(-)?]
        dev-python/lxml[>=1.3.6][python_abis:*(-)?]
        dev-python/numpy[>=1.1.0][python_abis:*(-)?]
        dev-python/scipy[python_abis:*(-)?]
        sci-libs/fftw[>=3.0]
        sci-libs/gsl[>=1.10]
        grc? (
            gnome-bindings/pygtk:2[>=2.10.0]
            dev-python/wxPython:2.8[python_abis:*(-)?]
        )
        qt4? (
            dev-python/PyQt4[>=4.10.0][python_abis:*(-)?]
            x11-libs/qt:4[>=4.2.0]
            x11-libs/qwt[>=6.1.1]
        )
    test:
        dev-cpp/cppunit[>=1.9.14]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -Wno-dev
    -DSYSCONFDIR=/etc
    -DENABLE_DEFAULT=OFF
    -DENABLE_TESTING=ON
    -DENABLE_PYTHON=ON
    -DENABLE_VOLK=ON
    -DENABLE_GNURADIO_RUNTIME=ON
    -DENABLE_GR_BLOCKS=ON
    -DENABLE_GR_FEC=ON
    -DENABLE_GR_FFT=ON
    -DENABLE_GR_FILTER=ON
    -DENABLE_GR_ANALOG=ON
    -DENABLE_GR_DIGITAL=ON
    -DENABLE_GR_DTV=ON
    -DENABLE_GR_ATSC=ON
    -DENABLE_GR_AUDIO=ON
    -DENABLE_GR_NOAA=ON
    -DENABLE_GR_PAGER=ON
    -DENABLE_GR_CHANNELS=ON
    -DENABLE_GR_TRELLIS=ON
    -DENABLE_GR_UTILS=ON
    -DENABLE_GR_WAVELET=ON
)

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=(
    'grc GRC'
    'grc GR_WXGUI'
    'qt4 GR_QTGUI'
)

src_prepare() {
    cmake_src_prepare

    # TODO: report upstream, multiarch fixes
    edo sed \
        -e '/set(GR_DATA_DIR/s:share:/usr/share:' \
        -e '/set(SYSCONFDIR/s: FORCE::' \
        -i CMakeLists.txt

    # skip tests
    # one needs network, the other running X, controlport tests fail to import python module
    edo echo \
"SET(CTEST_CUSTOM_TESTS_IGNORE
        qa_udp_source_sink
        qa_qtgui
        qa_trellis_test
        qa_cpp_py_binding
        qa_cpp_py_binding_set
        qa_ctrlport_probes
)"   > "${WORK}"/CTestCustom.cmake
}

src_install() {
    cmake_src_install

    edo find "${IMAGE}" -type d -empty -delete
}

src_test() {
    esandbox allow_net "inet:0.0.0.0@0"
    default
    esandbox disallow_net "inet:0.0.0.0@0"
}

