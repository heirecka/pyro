# Copyright 2013 Thomas Witt
# Distributed under the terms of the GNU General Public License v2

require github [ user=dunst-project tag=v${PV} ]

export_exlib_phases src_prepare

SUMMARY="Daemon for displaying notifications"
HOMEPAGE="http://www.knopwob.org/dunst/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS=""

BUGS_TO="pyromaniac@thwitt.de"

DEPENDENCIES="
    build:
        dev-lang/perl:* [[ description = [ pod2man is used to build the manpage ] ]]
        virtual/pkg-config
    build+run:
        dev-libs/glib:2[>=2.36]
        media-libs/freetype:*
        sys-apps/dbus
        x11-libs/cairo[X]
        x11-libs/gdk-pixbuf:2.0[X]
        x11-libs/libX11
        x11-libs/libxdg-basedir
        x11-libs/libXft
        x11-libs/libXinerama
        x11-libs/libXrandr[>=1.5.0]
        x11-libs/libXScrnSaver
        x11-libs/pango
"

DEFAULT_SRC_COMPILE_PARAMS=( PREFIX=/usr/$(exhost --target) )
DEFAULT_SRC_INSTALL_PARAMS=( PREFIX=/usr/$(exhost --target) )

dunst_src_prepare() {
    default

    # Multiarch
    edo sed -i -e "s:MANPREFIX = \${PREFIX}/share/man:MANPREFIX = /usr/share/man:" config.mk
    edo sed -i -e "s:-Os::" config.mk
    edo sed -i -e "s:pkg-config:$(exhost --tool-prefix)pkg-config:" config.mk
    edo sed -i -e "s:\${PREFIX}/share:/usr/share:" Makefile

    # Don't build dunstify, it needs libnotify as additional dependency
    # and doesn't get installed since it's only for testing
    edo sed -i -e 's/all: doc options dunst service dunstify/all: doc options dunst service/g' Makefile
}

